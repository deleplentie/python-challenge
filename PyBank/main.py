# import pandas to read csv file
import pandas as pd

# Load file
doc = pd.read_csv('Resources/budget_data.csv')

# Date row in data
doc_date = doc["Date"]
# profit/Losses Row in data
doc_pro_loss = doc["Profit/Losses"]

####~~~ TASK 1 ~~~####
print(f"Total Months: {len(doc_date)}")

####~~~ TASK 2 ~~~####
net_total = 0

for pro_loss in doc_pro_loss:
    net_total += pro_loss
print(f"Total: ${net_total}")


####~~~ TASK 3 ~~~####
##???????????????##

####~~~ TASKS 4 ~~~####
############ Doesn't tally with answer!!!!!!!!!!!!!!!!!!!!!!!!!!!
greatest_profit = 0
profit_date = str()
for date, profit_loss in zip(doc_date, doc_pro_loss):
    if greatest_profit < profit_loss:
        greatest_profit = profit_loss
        profit_date = date
print(f"{profit_date} (${greatest_profit})")
